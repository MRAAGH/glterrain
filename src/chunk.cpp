#include "chunk.h"

#include <glm/gtc/matrix_transform.hpp>

#include "globals.h"
#include "camera.h"

#include <iostream>
#include <vector>
#include <csignal>

using std::cout;
using std::endl;
using std::ostream;

int Chunk::numChunks = 0;

ostream& operator<<(ostream &os, const vec3 &vec){
    os << vec.x << " " << vec.y << " " << vec.z;
    return os;
}

float Chunk::getHeight(float x, float z){
    return 0;
}

Chunk::Chunk(std::vector<std::vector<float>> inputdata):
    chunkX(0), chunkZ(0)
{
    size = inputdata.size();
    numIndices = ((size / skip) - 1) * ((size / skip) - 1) * 6;
    data      = new vec3[sizeof(vec3) * size * size];
    indices   = new GLuint[sizeof(GLuint) * numIndices];
    normals   = new vec3[sizeof(vec3) * numIndices];

    {
        int index = 0;
        int offsetX = chunkX * (size - 1);
        int offsetZ = chunkZ * (size - 1);
        for(int j = 0; j < size; ++j){
            for(int i = 0; i < size; ++i){
                float x = i + offsetX;
                float z = j + offsetZ;

                data[index].x = x;
                data[index].y = inputdata[j][i]/2/256;
                data[index].z = z;
                ++index;
            }
        }
    }

    {
        int index = -1;
        for(int z = 0; z < size - 1; ++z){
            for(int x = 0; x < size - 1; ++x){

                int start = z * size + x;
                int TL = start;
                int TR = start + 1;
                int BL = start + size;
                int BR = start + 1 + size;

                if(abs(data[TR].y - data[BL].y) < abs(data[TL].y - data[BR].y)){
                    indices[++index] = TR;
                    indices[++index] = BL;
                    indices[++index] = BR;

                    indices[++index] = TR;
                    indices[++index] = TL;
                    indices[++index] = BL;
                } else {
                    indices[++index] = TL;
                    indices[++index] = BL;
                    indices[++index] = BR;

                    indices[++index] = TR;
                    indices[++index] = TL;
                    indices[++index] = BR;
                }

            }
        }

    }

    {
        for(int i = 0; i < numIndices; i += 3){
            vec3 &v0 = data[indices[i + 0]];
            vec3 &v1 = data[indices[i + 1]];
            vec3 &v2 = data[indices[i + 2]];

            vec3 normal = normalize(cross(v1 - v0, v2 - v0));

            normals[indices[i + 0]] += normal;
            normals[indices[i + 1]] += normal;
            normals[indices[i + 2]] += normal;
        }
        for(unsigned int i = 0; i < numIndices; ++i){
            normals[i] = normalize(normals[i]);
        }
    }

}

void Chunk::upload(){
    //remove?
    glBindVertexArray(0);
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, size * size * sizeof(vec3), data, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(vec3), 0);
    glEnableVertexAttribArray(0);

    glGenBuffers(1, &normalBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, normalBuffer);
    glBufferData(GL_ARRAY_BUFFER, numIndices * sizeof(vec3), normals, GL_STATIC_DRAW);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(vec3), 0);
    glEnableVertexAttribArray(1);

    glGenBuffers(1, &ebo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, numIndices * sizeof(GLuint), indices, GL_STATIC_DRAW);

    delete[] data;
    delete[] normals;
    delete[] indices;
}

Chunk::~Chunk(){
    --numChunks;
    glDeleteBuffers(1, &vbo);
    glDeleteBuffers(1, &ebo);
    glDeleteVertexArrays(1, &vao);
}

void Chunk::render() const {
    glBindVertexArray(vao);
    glDrawElements(GL_TRIANGLES, numIndices, GL_UNSIGNED_INT, 0);
    glBindVertexArray(0);
}
