#include "engine.h"
#include <iostream>

int main(int argc, char** argv){
    if (argc < 2) {
        std::cerr << "Usage: glterrain <hmapfile>" << std::endl;
        return 1;
    }
    char* fname = argv[1];
    Engine eng(fname);
    eng.start();
    return 0;
}
