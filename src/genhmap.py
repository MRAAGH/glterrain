#!/usr/bin/python3

import math

n = 512
print(n)
for i in range(n):
    for j in range(n):
        x = i/10
        y = j/10
        z = math.sin(x**1.8/70)+math.sin(y/10)+2
        # z is between 0 and 4
        # then gets scaled between 0 and 65536
        print(int(z*64*256))
