#include "globals.h"

GLFWwindow *window;

namespace Screen {
    //const int width = 1280;
    //const int height = 720;
    /* const int width = 1600; */
    /* const int height = 900 - 32; */
    const int width = 1920;
    const int height = 1080;

    int displayW;
    int displayH;
    float aspectRatio;
}

namespace Input {
    unique_ptr<Keyboard> keyboard;
    unique_ptr<Mouse> mouse;
}

namespace Shaders {
    unique_ptr<Shader> terrain;
    mat3 vars;
    GLuint texture[];
    GLuint textureLoc[];
}

namespace Globals {
    void init(){
        Input::keyboard = unique_ptr<Keyboard>(new Keyboard());
        Input::mouse = unique_ptr<Mouse>(new Mouse());
        Shaders::terrain = unique_ptr<Shader>(new Shader("shaders/shader.vert", "shaders/shader.frag"));
        //Shaders::terrainNorm = unique_ptr<Shader>(new Shader("shaders/normshader.vert", "shaders/normshader.frag", "shaders/normshader.geo"));

        glfwGetFramebufferSize(window, &Screen::displayW, &Screen::displayH);
        Screen::aspectRatio = static_cast<float>(Screen::displayW) / Screen::displayH;
        //Screen::aspectRatio = static_cast<float>(Screen::width) / Screen::height;

        Shaders::vars[1].x = 0.2f;
        Shaders::vars[1].y = 0.5f;

    }
}

namespace Time {
    double oldTime;
    double deltaTime;

    void updateTime(){
        double time = glfwGetTime();
        deltaTime = time - oldTime;
        oldTime = time;
    }
}
