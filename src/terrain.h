#ifndef TERRAIN_H
#define TERRAIN_H

#include <memory>
#include <mutex>
#include <vector>

#include "chunk.h"

using std::mutex;
using std::unique_ptr;

class Terrain {
    public:
        Terrain(char* fname);
        void render();
    private:
        void chunker();
        void reader(char* fname);

        const static int size = 1;
        unique_ptr<Chunk> chunks[size * size];
        unique_ptr<Chunk> nextChunk;

        mutex mtx;

        std::vector<std::vector<float>> inputdata;
};

#endif
