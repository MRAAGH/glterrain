# glterrain

forked from [sealj553/glterrain](https://github.com/sealj553/glterrain)

Displays a heightmap from file or standard input. Uses OpenGL 3.

Dependencies:
* OpenGL
* GLEW
* GLFW
* SOIL

Uses:
* [Imgui](https://github.com/ocornut/imgui)

Screenshot:
![screenshot](screenshot.png)

Compile:
```
cd src
make
```

Run (from file):
```
./glterrain hmapexample.txt
```

Run (from generator):
```
./genhmap.py | ./glterrain -
```
